python-numpy-groupies (0.10.2-2) UNRELEASED; urgency=medium

  * Team upload.
  * Use autopkgtest-pkg-pybuild testsuite.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 10 Jan 2024 14:31:45 +0100

python-numpy-groupies (0.10.2-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Team upload.
  * New upstream release.
    (closes: #1058133)
  * Add python3-setuptools-scm to build dependencies.
  * Drop patches applied upstream.
  * Add pybuild-plugin-pyproject to build dependencies.
  * Update copyright file.
  * Update lintian overrides.
  * Override dh_python3 to set shebang to /usr/bin/python3.
  * Switch to dh-sequence-*.
  * Don't override clean target.
  * Bump Standards-Version to 4.6.2, no changes.
  * Add gbp.conf to use pristine-tar & --source-only-changes by default.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 10 Jan 2024 11:40:12 +0100

python-numpy-groupies (0.9.20-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    (closes: #1026885, #1027196, #1027234)
  * Sort build dependencies.
  * Drop obsolete python3-pytest-runner build dependency.
  * Drop python3-distutils build dependency.
  * Add patch to not use broken Clean class.
  * Build without python3-numba (RC buggy).
  * Bump Standards-Version to 4.6.1, no changes.
  * Add patch to fix versioneer configuration.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 07 Jan 2023 20:03:30 +0100

python-numpy-groupies (0.9.13-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 03 Feb 2021 13:14:23 +0100

python-numpy-groupies (0.9.10-2) unstable; urgency=medium

  * Team upload.
  * DEP3
  * Enhance description
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test
  * Section: python
  * cme fix dpkg-control
  * Testsuite: autopkgtest-pkg-python
  * Add versioneer.py to debian/copyright
  * <!nocheck> for Build-Depends only needed in tests
  * d/rules: Remove 'python-' from PYBUILD_NAME

 -- Andreas Tille <tille@debian.org>  Sat, 28 Mar 2020 07:00:58 +0100

python-numpy-groupies (0.9.10-1) unstable; urgency=medium

  * Initial release (Closes: #948184)

 -- Steffen Moeller <moeller@debian.org>  Sun, 05 Jan 2020 01:01:47 +0100
